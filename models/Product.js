const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

    name: {
        type: String,
        required: [true, "Name is Required"]
    },
    description: {
        type: String,
        required: [true, "Description is Required"]
    },
    category: {
        type: String,
        required: [true, "Category is Required"]
    },
    size: {
        type: String,
        required: [true, "Size is Required"]
    },
    price: {
        type: Number,
        required: [true, "Price is Required"]
    },
    stocks: {
        type: Number,
        required: [true, "Number of Stocks is Required"]
    },
    photos: {
        type: String,
        default: "pending"
    },
    isActive: {
        type: Boolean,
        required: true,
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [

        {
            orderId: {
                type: String,
                required: [true, "Order ID is required"]
            },
            qtyOrdered: {
                type: Number,
                required: true
            },
            status: {
                type: String,
                default: "Ongoing"
            },
            orderedOn: {
                type: Date,
                default: new Date()
            }
        }

    ],
    productReview: [

        {
            userEmail:{
                type: String,
            },
            orderID:{
                type: String,
            },
            feedback:{
                type: String,
            },
            rate:{
                type: Number,
            },
            reviewedOn: {
                type: Date,
                default: new Date()
            }
        }

    ]

});

module.exports = mongoose.model("Product", productSchema);