const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: [true, "First Name is Required"]
    },
    lastName: {
        type: String,
        required: [true, "Last Name is Required"]
    },
    mobileNo: {
        type: String,
        required: [true, "Please enter a valid mobile number"]
    },
    email: {
        type: String,
        required: [true, "Please enter a valid email."]
    },
    password: {
        type: String,
        required: [true, "Please enter a valid password."]
    },
    gender: {
        type: String
    },
    birthday: {
        type: String
    },
    address: {
        houseNo: {
            type: String
        },
        street: {
            type: String
        },
        barangay: {
            type: String
        },
        city: {
            type: String
        },
        province: {
            type: String
        },
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isMember: {
        type: Boolean,
        default: false
    },
    registeredOn: {
        type: Date,
        default: new Date()
    },
    orders: [

        {
            totalAmount: {
                type: Number,
                required: true
            },
            orderedOn: {
                type: Date,
                default: new Date()
            },
            products: [

                {
                    productId: {
                        type: String,
                        required: [true, "Product ID is required"]
                    },
                    quantity: {
                        type: Number,
                        required: true,
                        min: [1, "Quantity can not be less than 1"]
                    },
                    price: {
                        type: Number,
                        required: true
                    },
                    orderedOn: {
                        type: Date,
                        default: new Date()
                    }
                }

            ]

        }

    ],
    orderReview: [

        {
            productId: {
                type: String,
                required: [true, "Product ID is required"]
            },
            orderID: {
                type: String,
            },
            feedback: {
                type: String,
            },
            rate: {
                type: Number,
                max: [5, "Quantity can not be greater than 5"]
            },
            reviewedOn: {
                type: Date,
                default: new Date()
            }
        }

    ]

});

module.exports = mongoose.model("User", userSchema);