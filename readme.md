Project Name: Lara's Online Shoppe E-Commerce API

Features:
User Registration,
User Authentication,
Set user as admin, 
Deactivate user admin, 
Activate Membership,
Deactivate Membership,
CheckOut,
Cancel Order, 
Feedback and Rate Order,
//Delete Feedback and Rate Order,* 
Get User Order,
View All Orders,
Check Email Exists,
Add Product, 
View All Active Products,
View All Products,
View All Archive Products,
Get Single Product,
Update Product,
Archive Product,
Activate Product,
View All Mens Item,
View All Womens Item,
View All Tops Item,
View All Bottoms Item,
View All Accessories Item,
View All MensTops Item,
View All MensBottoms Item,
View All MensAccs Item,
View All WomensTops Item,
View All WomensBottoms Item,
View All WomensAccs Item,
View Products price from Lowest to Highest,
View Products price from Highest to Lowest,
Delete User Account,
Delete Product,
Update User




Routes and Request Body:

----------Registration---------- 

POST - http://localhost:4000/users/registration

Body: (JSON)
{
		"firstName": "String",
        "lastName": "String",
        "mobileNo": "String",
        "email": "String",
        "password": "String",
        "gender": "String",
        "birthday": "String",
        "address": "String"
}

----------Login---------- 

POST - http://localhost:4000/users/login

Body: (JSON)
{
    "email": "String",
    "password": "String"
}

----------Set as Admin----------

PUT - http://localhost:4000/users/activateAdmin/:id

Body: No Request Body
Admin Token Required.

Admin Credentials:

email: "adminAPI@gmail.com",
password: "adminAPI123"

----------Deactivate user admin----------

PUT -http://localhost:4000/users/deactivateAdmin/:id

Body: No Request Body
Admin Token Required


----------Checkout----------

POST - http://localhost:4000/users/checkOut

Body: (JSON)
{
    "totalAmount": Number,
    "products":[
        {
            "productId": "String",
            "quantity": Number,
            "price": Number
        }
        ]
}
User Token Required

----------Cancel Order----------

DELETE - http://localhost:4000/users/cancelCheckout

Body: (JSON)

{
    "_id": "String"
}
User Token Required

          
----------Feedback and Rate Order----------

POST - http://localhost:4000/users/feedbackAndRateOrder

Body: (JSON)

{
    "productId": "String",
    "orderID": "String",
    "feedback": "String",
    "rate": number
   
}
User Token Required

<!-- ----------Delete Feedback and Rate Order----------

DELETE - http://localhost:4000/users/deleteFeedbackAndRateOrder

Body: (JSON)

{
    "_id": "String"
}
User Token Required -->

----------Get User Order----------

GET - http://localhost:4000/users/getUserOrder/:id

Body: No Request Body
User Token Required

----------View All Orders----------

GET - http://localhost:4000/users/viewAllOrders

Body: No Request Body
Admin Token Required

----------Check Email Exists----------

POST - http://localhost:4000/users/checkEmailExists

{
    "email": "String"
}

----------Add Product----------

POST - http://localhost:4000/products

{
    "name": "String",
    "description": "String",
    "category": "String",
    "size": "String",
    "price": number,
    "stocks": number
}
Admin Token Required

----------View All Active Products----------

GET - http://localhost:4000/products/getAllActive

Body: No Request Body

----------View All Products----------

GET - http://localhost:4000/products/getAllProducts

Body: No Request Body
Admin Token Required

----------View All Archive Products----------

GET - http://localhost:4000/products/getAllArchive

Body: No Request Body
Admin Token Required

----------Get Single Product----------

GET - http://localhost:4000/products/getSingleProduct/:id

Body: No Request Body

----------Update Product----------

PUT - http://localhost:4000/products/updateProduct/:id

{
        "name": "String",
        "description": "String",
        "category": "String",
        "size:" "String",
        "price": number,
        "stocks": number
}

Admin Token Required

----------Archive Product----------

PUT - http://localhost:4000/products/archiveProduct/:id

Body: No Request Body
Admin Token Required

----------Activate Product----------

PUT - http://localhost:4000/products/activateProduct/:id

Body: No Request Body
Admin Token Required

----------View All Mens Item----------

GET - http://localhost:4000/products/viewAllMensItem

Body: No Request 

----------View All Womens Item----------

GET - http://localhost:4000/products/viewAllWomensItem

Body: No Request Body

----------View All Tops Item----------

GET - http://localhost:4000/products/viewAllTopsItem

Body: No Request Body

----------View All Bottoms Item----------

GET - http://localhost:4000/products/viewAllBottomsItem

Body: No Request Body

----------View All Accessories Item----------

GET - http://localhost:4000/products/viewAllAccsItem

Body: No Request Body

----------View All MensTops Item----------

GET - http://localhost:4000/products/viewAllMensTopsItem

Body: No Request Body

----------View All MensBottoms Item----------

GET - http://localhost:4000/products/viewAllMensBottomsItem

Body: No Request Body

----------View All MensAccs Item----------

GET - http://localhost:4000/products/viewAllMensAccsItem

Body: No Request Body

----------View All WomensTops Item----------

GET - http://localhost:4000/products/viewAllWomensTopsItem

Body: No Request Body

----------View All WomensBottoms Item----------

GET - http://localhost:4000/products/viewAllWomensBottomsItem

Body: No Request Body

----------View Products price from Lowest to Highest----------

GET - http://localhost:4000/products/ViewProductsFromLowPricetoHighest

Body: No Request Body

----------View Products price from Highest to Lowest----------

GET - http://localhost:4000/products/ViewProductsFromHighPricetoLowest

Body: No Request Body

----------Delete User Account----------

DELETE - http://localhost:4000/users/deleteUser/:id

Body: No Request Body
Admin Token Required

----------Delete Product----------

DELETE - http://localhost:4000/products/deleteProduct/:id

Body: No Request Body
Admin Token Required

----------Update User----------

PUT - http://localhost:4000/users/updateUser/:id

{
    "firstName": "String",
    "lastName": "String",
    "mobileNo": "String",
    "address": {
             "houseNo": "String",
             "street": "String",
             "barangay": "String",
             "city": "String",
             "province": "String"
         }
}

User Token Required