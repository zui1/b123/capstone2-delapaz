const express = require("express");

const router = express.Router();

const userControllers = require('../controllers/userControllers');

const {
    registerUser,
    loginUser,
    updateUser,
    deleteUser,
    activateUserAdmin,
    deactivateUserAdmin,
    activateMembership,
    deactivateMembership,
    checkOut,
    cancelOrder,
    feedbackAndRateOrder,
    //deleteFeedbackAndRateOrder,
    getUserOrder,
    viewAllOrders,
    checkEmailExists
} = userControllers

const auth = require('../auth');

const { verify, verifyAdmin } = auth;

router.post('/registration', registerUser);

router.post('/login', loginUser);

router.put('/updateUser/:id', verify, updateUser);

router.delete('/deleteUser/:id', verify, verifyAdmin, deleteUser);

router.put('/activateAdmin/:id', verify, verifyAdmin, activateUserAdmin);

router.put('/deactivateAdmin/:id', verify, verifyAdmin, deactivateUserAdmin);

router.put('/activateMembership', verify, activateMembership);

router.put('/deactivateMembership/:id', verify, verifyAdmin, deactivateMembership);

router.post('/checkOut', verify, checkOut);

router.delete('/cancelOrder', verify, cancelOrder);

router.post('/feedbackAndRateOrder', verify, feedbackAndRateOrder);

//router.delete('/deleteFeedbackAndRateOrder', verify, deleteFeedbackAndRateOrder);

router.get('/getUserOrder/:id', verify, getUserOrder);

router.get('/viewAllOrders', verify, verifyAdmin, viewAllOrders);

router.post('/checkEmailExists', checkEmailExists);

module.exports = router;