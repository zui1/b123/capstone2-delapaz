const express = require("express");

const router = express.Router();

const productControllers = require('../controllers/productControllers');

const {
    addProduct,
    viewAllActiveProducts,
    viewAllProducts,
    viewAllArchiveProducts,
    getSingleProduct,
    updateProduct,
    deleteProduct,
    archiveProduct,
    activateProduct,
    //updateStatusOfCancelledOrder,
    viewAllMensItem,
    viewAllWomensItem,
    viewAllTopsItem,
    viewAllBottomsItem,
    viewAllAccessoriesItem,
    viewAllMensTopsItem,
    viewAllMensBottomsItem,
    viewAllMensAccsItem,
    viewAllWomensTopsItem,
    viewAllWomensBottomsItem,
    viewAllWomensAccsItem,
    viewProductsFromLowPricetoHighest,
    viewProductsFromHighPricetoLowest
} = productControllers

const auth = require('../auth');

const { verify, verifyAdmin } = auth;

router.post('/', verify, verifyAdmin, addProduct);

router.get('/getAllActive', viewAllActiveProducts);

router.get('/getAllProducts', verify, verifyAdmin, viewAllProducts);

router.get('/getAllArchive', verify, verifyAdmin, viewAllArchiveProducts);

router.get('/getSingleProduct/:id', getSingleProduct);

router.put("/updateProduct/:id", verify, verifyAdmin, updateProduct);

router.delete("/deleteProduct/:id", verify, verifyAdmin, deleteProduct);

router.put("/archiveProduct/:id", verify, verifyAdmin, archiveProduct);

router.put("/activateProduct/:id", verify, verifyAdmin, activateProduct);

//router.put("/updateStatusOfCancelledOrder/:id", verify, verifyAdmin, updateStatusOfCancelledOrder);

router.get('/viewAllMensItem', viewAllMensItem);

router.get('/viewAllWomensItem', viewAllWomensItem);

router.get('/viewAllTopsItem', viewAllTopsItem);

router.get('/viewAllBottomsItem', viewAllBottomsItem);

router.get('/viewAllAccessoriesItem', viewAllAccessoriesItem);

router.get('/viewAllMensTopsItem', viewAllMensTopsItem);

router.get('/viewAllMensBottomsItem', viewAllMensBottomsItem);

router.get('/viewAllMensAccsItem', viewAllMensAccsItem);

router.get('/viewAllWomensTopsItem', viewAllWomensTopsItem);

router.get('/viewAllWomensBottomsItem', viewAllWomensBottomsItem);

router.get('/viewAllWomensAccsItem', viewAllWomensAccsItem);

router.get('/viewProductsFromLowPricetoHighest', viewProductsFromLowPricetoHighest);

router.get('/viewProductsFromHighPricetoLowest', viewProductsFromHighPricetoLowest);

module.exports = router;