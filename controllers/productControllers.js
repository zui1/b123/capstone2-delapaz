const Product = require('../models/Product');

module.exports.addProduct = (req, res) => {

    Product.findOne({ name: req.body.name })
        .then(result => {

            if (result !== null && result.name === req.body.name) {

                return res.send("Product already Added.");

            } else {

                let newProduct = new Product({

                    name: req.body.name,
                    description: req.body.description,
                    category: req.body.category,
                    size: req.body.size,
                    price: req.body.price,
                    stocks: req.body.stocks,
                    isActive: true

                })

                let newProduct1 = new Product({

                    name: req.body.name,
                    description: req.body.description,
                    category: req.body.category,
                    size: req.body.size,
                    price: req.body.price,
                    stocks: req.body.stocks,
                    isActive: false

                })

                if (req.body.stocks > 0) {

                    newProduct.save()
                        .then(result => res.send({

                            message: "Product/s added Succesfully",

                        }))
                        .catch(err => res.send(err))

                } else {

                    newProduct1.save()
                        .then(result => res.send({

                            message: "Product/s added Succesfully",

                        }))
                        .catch(err => res.send(err))

                }

            }

        })

}

module.exports.viewAllActiveProducts = (req, res) => {

    Product.find({ isActive: true }, { _id: 1, name: 1, description: 1, category: 1, size: 1, price: 1, photos: 1, productReview: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))


}

module.exports.viewAllProducts = (req, res) => {

    Product.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))


}

module.exports.viewAllArchiveProducts = (req, res) => {

    Product.find({ isActive: false })
        .then(result => res.send(result))
        .catch(err => res.send(err))


}

module.exports.getSingleProduct = (req, res) => {

    Product.findById(req.params.id)
        .then(product_details => res.send({ product_details }))
        .catch(err => res.send(err))

}

module.exports.updateProduct = (req, res) => {

    let updateProd = {

        name: req.body.name,
        description: req.body.description,
        category: req.body.category,
        size: req.body.size,
        price: req.body.price,
        stocks: req.body.stocks

    }

    Product.findByIdAndUpdate(req.params.id, updateProd, { new: true })
        .then(product_details => res.send({

            message: "Product succesfully updated.",

        }))
        .catch(err => res.send(err))

}

module.exports.deleteProduct = (req, res) => {

    Product.findByIdAndDelete(req.params.id)
        .then(result => res.send({

            message: "Product Deleted.",

        }))
        .catch(err => res.send(err))

}

module.exports.archiveProduct = (req, res) => {

    let archiveProd = {

        isActive: false

    }

    Product.findByIdAndUpdate(req.params.id, archiveProd, { new: true })
        .then(result => res.send({

            message: "Product archived.",

        }))
        .catch(err => res.send(err))

}

module.exports.activateProduct = (req, res) => {

    let activateProd = {

        isActive: true

    }

    Product.findByIdAndUpdate(req.params.id, activateProd, { new: true })
        .then(result => res.send({

            message: "Product activated.",

        }))
        .catch(err => res.send(err))

}

/*module.exports.updateStatusOfCancelledOrder = (req, res) => {

    let updateStatus = {

        orders: req.body.orders,
   
    }

    Product.findByIdAndUpdate(req.params.id, updateStatus, { new: true })
        .then(status_update => res.send(status_udpate))
        .catch(err => res.send(err))

}*/

module.exports.viewAllMensItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "Men" } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllWomensItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "women" } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllTopsItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "top", $options: '$i' } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllBottomsItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "Bottom", $options: '$i' } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllAccessoriesItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "accs", $options: '$i' } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllMensTopsItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "MensTop" } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllMensBottomsItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "MensBottom" } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllMensAccsItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "MensAccs" } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllWomensTopsItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "womensTop" } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllWomensBottomsItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "womensBottom" } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewAllWomensAccsItem = (req, res) => {

    Product.find({ $and: [{ "category": { $regex: "womensAccs" } }, { "isActive": true }] }, { _id: 0, name: 1, description: 1, category: 1, size: 1, price: 1, stocks: 1, photos: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewProductsFromLowPricetoHighest = (req, res) => {

    Product.find({}).sort({ price: 1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.viewProductsFromHighPricetoLowest = (req, res) => {

    Product.find({}).sort({ price: -1 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}