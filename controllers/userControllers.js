const User = require('../models/User');

const Product = require('../models/Product');

const bcrypt = require('bcrypt');

const auth = require('../auth');

const { createAccessToken } = auth

module.exports.registerUser = (req, res) => {

    if (req.body.password.length < 6) return res.send({ message: "Insufficient password." })

    const hashedPW = bcrypt.hashSync(req.body.password, 10);

    let newUser = new User({

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo,
        email: req.body.email,
        password: hashedPW,
        gender: req.body.gender,
        birthday: req.body.birthday,
        address: req.body.address

    })

    newUser.save()
        .then(result => res.send({

            message: "You have successfully registered"

        }))
        .catch(err => res.send(err))


}

module.exports.loginUser = (req, res) => {

    User.findOne({ email: req.body.email })
        .then(result => {

            if (result === null) {

                return res.send({ message: "User does not exist." })

            } else {

                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
                console.log(isPasswordCorrect);

                if (isPasswordCorrect) {

                    return res.send({
                        message: "You have successfully logged in.",
                        accessToken: createAccessToken(result)
                    });

                } else {

                    return res.send({ message: "Invalid password." })

                }

            }

        })
        .catch(err => res.send(err))

}

module.exports.updateUser = (req, res) => {

    let updateUsr = {

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo,
        address: req.body.address

    }

    User.findByIdAndUpdate(req.params.id, updateUsr, { new: true })
        .then(user_details => res.send({

            message: "Account Information succesfully updated.",

        }))
        .catch(err => res.send(err))

}

module.exports.deleteUser = (req, res) => {

    User.findByIdAndDelete(req.params.id)
        .then(result => res.send({

            message: "Account Deleted.",

        }))
        .catch(err => res.send(err))

}

module.exports.activateUserAdmin = (req, res) => {

    let updateIsAdmin = {

        isAdmin: true

    }

    User.findByIdAndUpdate(req.params.id, updateIsAdmin, { new: true })
        .then(result => res.send({

            message: "Successfully assigned as Admin",

        }))
        .catch(err => res.send(err))

}

module.exports.deactivateUserAdmin = (req, res) => {

    let updateIsAdmin = {

        isAdmin: false

    }

    User.findByIdAndUpdate(req.params.id, updateIsAdmin, { new: true })
        .then(result => res.send({

            message: "Successfully updated user's isAdmin role.",

        }))
        .catch(err => res.send(err))

}

module.exports.activateMembership = (req, res) => {

    let activateMembership = {

        isMember: true

    }

    User.findByIdAndUpdate(req.user.id, activateMembership, { new: true })
        .then(result => res.send({

            message: "Thank you for Subscribing! Enjoy the perks of being a Member, Order now!",

        }))
        .catch(err => res.send(err))

}

module.exports.deactivateMembership = (req, res) => {

    let deactivateMembership = {

        isMember: false

    }

    User.findByIdAndUpdate(req.params.id, deactivateMembership, { new: true })
        .then(result => res.send({

            message: "Successfully deactivated user's Membership.",
            details: result

        }))
        .catch(err => res.send(err))

}

module.exports.checkOut = async (req, res) => {

    if (req.user.isAdmin) return res.send({

        auth: "Failed",
        message: "Action Forbidden"

    });


    let updateUser = await User.findById(req.user.id).then(user => {


        if (user.isMember === true) {

            //console.log(req.body)
            //console.log(req.body.products)

            user.orders.push({

                totalAmount: req.body.totalAmount - req.body.totalAmount * .05,
                products: req.body.products

            })


            return user.save()
                .then(user => {

                    return user.orders[user.orders.length - 1]._id

                })
                .catch(err => false)

        } else {

            user.orders.push({

                totalAmount: req.body.totalAmount,
                products: req.body.products

            })


            return user.save()
                .then(user => {

                    return user.orders[user.orders.length - 1]._id

                })
                .catch(err => false)

        }


    })

    if (updateUser === false) {
        res.send({ message: "Something went wrong." })
    }
    console.log(updateUser)

    req.body.products.forEach(result => {

        //console.log(result)
        //console.log(result.productId)
        //console.log(result.quantity)
        Product.findById(result.productId).then(product => {

            //console.log(product)

            product.orders.push({

                orderId: updateUser,
                qtyOrdered: result.quantity

            })

            return product.save()
                .then(product => console.log(product))
                .catch(err => res.send(err))

        })


    })

    req.body.products.forEach(result => {

        Product.findById(result.productId).then(product => {

            let updateStocks = (product.stocks - result.quantity)
            product.stocks = updateStocks

            return product.save()
                .then(product => console.log(product))
                .catch(err => res.send(err))

        })

    })


    res.send({ message: "Checkout Complete! Thank you for purchasing" })
}

module.exports.cancelOrder = (req, res) => {

    User.findById(req.user.id).then(user => {


        user.orders.pull({

            _id: req.body._id,
            products: req.body.products

        })

        return user.save()
            .then(result => res.send({

                message: "Order Cancelled."

            }))
            .catch(err => res.send(err))

    })

    req.body.products.forEach(result => {

        Product.findById(result.productId).then(product => {
            //console.log(product.orders)
            let updateStatus = product.orders
            //console.log(updateStatus[updateStatus.length - 1].status)

            let updatedStatus = "Cancelled"
            updateStatus[updateStatus.length - 1].status = updatedStatus
            return product.save()
                .then(result => console.log(result))
                .catch(err => res.send(err))

        })


    })

    req.body.products.forEach(result => {

        Product.findById(result.productId).then(product => {
            //console.log(result.quantity)
            // console.log(product.stocks)
            let updateStocks = (product.stocks + result.quantity)
            product.stocks = updateStocks

            return product.save()
                .then(product => console.log(product))
                .catch(err => res.send(err))

        })

    })

}

module.exports.feedbackAndRateOrder = async (req, res) => {

    if (req.user.isAdmin) return res.send({

        auth: "Failed",
        message: "Action Forbidden"

    });

    let postUserReviews = await User.findById(req.user.id).then(user => {

        user.orderReview.push({

            productId: req.body.productId,
            orderID: req.body.orderID,
            feedback: req.body.feedback,
            rate: req.body.rate

        })

        return user.save()
            .then(user => true)
            .catch(err => err.message)

    })

    if (postUserReviews !== true) return res.send(postUserReviews);

    let postProductReviews = await Product.findById(req.body.productId).then(product => {

        product.productReview.push({

            userEmail: req.user.email,
            orderID: req.body.orderID,
            feedback: req.body.feedback,
            rate: req.body.rate

        })

        return product.save()
            .then(user => true)
            .catch(err => err.message)

    })

    if (postProductReviews !== true) return res.send(postProductReviews);

    if (postUserReviews && postProductReviews) return res.send("Feedback and Rate posted.");

}

/*module.exports.deleteFeedbackAndRateOrder = (req, res) => {

    User.findById(req.user.id).then(user => {


        user.orderReview.pull({ _id: req.body._id })

        return user.save()
            .then(result => res.send({

                message: "Order Review Deleted."

            }))
            .catch(err => res.send(err))

    })
    //console.log(req.body._id)

    req.body.products.forEach(result => {

        Product.findById(result.productId).then(product => {

            let updateStatus = product.productReview

            let updatedStatus = "Deleted"
            updateStatus[updateStatus.length - 1].status = updatedStatus
            return product.save()
                .then(result => console.log(result))
                .catch(err => res.send(err))

        })

    })

}*/

module.exports.getUserOrder = (req, res) => {

    User.findById(req.params.id)
        .then(order_details => res.send(order_details.orders))
        .catch(err => res.send(err))

}

module.exports.viewAllOrders = (req, res) => {

    User.find({ "isAdmin": false }, { _id: 0, firstName: 0, lastName: 0, mobileNo: 0, password: 0, isAdmin: 0, isMember: 0, registeredOn: 0, orderReview: 0, __v: 0, birthday: 0, gender: 0, address: 0 })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.checkEmailExists = (req, res) => {

    User.findOne({ email: req.body.email })
        .then(result => {

            if (result === null) {

                return res.send({
                    message: "Email Available."
                })

            } else {

                return res.send({
                    message: "Email is already registered."
                })

            }
        })
        .catch(err => res.send(err))
}