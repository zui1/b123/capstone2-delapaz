const express = require("express");

const mongoose = require('mongoose');

const cors = require('cors')

const app = express();

const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://arnondp:arnondp123@cluster0.t5pqi.mongodb.net/e-commerceAPI?retryWrites=true&w=majority",

    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }

);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Connected to MongoDB"))

app.use(express.json());

const userRoutes = require('./routes/userRoutes')
app.use('/users', userRoutes);

const productRoutes = require('./routes/productRoutes')
app.use('/products', productRoutes);



app.listen(port, () => console.log(`Server is running at port ${port}`));